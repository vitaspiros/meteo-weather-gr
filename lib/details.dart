import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:meteoweather/globals.dart';
import 'data.dart';

class DetailedView extends StatefulWidget {
  @override
  State<DetailedView> createState() => _DetailedViewState();
}

class _DetailedViewState extends State<DetailedView> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(future: getData(Globals.cityId), builder: (context, snapshot) {

          bool hasFinished = snapshot.hasData && snapshot.connectionState != ConnectionState.waiting;
          if (!hasFinished) {
            return const Center(child: CircularProgressIndicator());
          }

          if (snapshot.hasError) {
            return const Center(child: Column(children: [
              Icon(Icons.error, size: 48),
              Text("Something went wrong. Check your internet connection."),
              //Text(snapshot.error.toString())
            ],));
          }
          return RefreshIndicator(
              child: Column(
                children: [
                  FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Padding(
                      padding: const EdgeInsets.all(65.0),
                      child: Text(snapshot.data!["temp"], style: const TextStyle(fontSize: 112)),
                    ),
                  ),
                  Expanded(
                    child: ListView(
                      children: <Widget>[
                        //_WeatherCard(snapshot.hasData ? snapshot.data!["temp"] : snapshot.error.toString(), const Icon(Icons.thermostat, size: 48)),
                        _WeatherCard(snapshot.hasData ? snapshot.data!["status"] : snapshot.error.toString(), const Icon(Icons.cloud_outlined, size: 48)),
                        _WeatherCard(snapshot.hasData ? snapshot.data!["humidity"] : snapshot.error.toString(), const Icon(Icons.water_drop, size: 48, color: Colors.blue)),
                        _WeatherCard(snapshot.hasData ? snapshot.data!["pressure"] : snapshot.error.toString(), const Icon(Icons.compress, size: 48)),
                        _WeatherCard(snapshot.hasData ? snapshot.data!["windSpeed"] : snapshot.error.toString(), const Icon(Icons.wind_power, size: 48, color: Colors.blueAccent))
                      ]
                    )
                  )
                ]
            ),

            onRefresh: () async {
              await DefaultCacheManager().removeFile("html${Globals.cityId}").then((v) {
                setState(() {

                });
              });
            }
          );
        });
  }
}

class _WeatherCard extends StatelessWidget {
  final String text;
  final Icon icon;

  const _WeatherCard(this.text, this.icon);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100,
      child: Card.filled(
        child: FittedBox(
          child: Row(
            children: [
              icon,
              Text(text, style: const TextStyle(fontSize: 48))
            ]
          )
        )
      )
    );
  }
}